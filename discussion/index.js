// Assignment Operators

// Basic Assignment Operator (=) 
// The assignment operator adds the value of the
// right operand to a variable and assigns the result to the variable


let assignmentNumner = 8;
console.log(assignmentNumner); //8

// Addition Assignment Operator

let totalNumber = assignmentNumner + 2;
console.log("Result of addition assignment operator: " + totalNumber);

// Arithmetic Operator (+, -, *, /, %)

let x = 2;
let y = 5;

let sum = x + y;
console.log("Result of addition operator: " + sum);

let difference = x - y;
console.log("Result of subtraction operator: " + difference);

let product = x * y;
console.log("Result of multiplication operator: " + product);

let quotient = y / x;
console.log("Result of division operator: " + quotient);

let remainder = y % x; // 5 % 2
console.log("Result of modulo operator: " + remainder);

// Mini Activity:

/*- When multiple operators are applied in a single statement, it follows the PEMDAS (Parenthesis, Exponents, Multiplication, Division, Addition and Subtraction) rule
            - The operations were done in the following order:
                1. 3 * 4 = 12
                2. 12 / 5 = 2.4
                3. 1 + 2 = 3
                4. 3 - 2.4 = 0.6*/

let mDas = 1 + 2 - 3 * 4 / 5;

let mDas2 = 6 / 2 * (1 + 2);
console.log("Result of mDas operation: " + mDas);
console.log("Result of mDas operation: " + mDas2);

let pemDas = 1 + (2 - 3) * (4 / 5);


console.log(pemDas);

// Equality Operator
let juan = 'juan';

console.log(1 == 1);
console.log(1 == 2);
console.log(1 == '1');
console.log(0 == false);
console.log('juan' == 'juan');
console.log('juan' == juan);

console.log(1 != 1);
console.log(1 != 2);
console.log(1 != '1');
console.log(0 != false);
console.log('juan' != 'juan');
console.log('juan' != juan);

console.log(1 === 1);
console.log(1 === 2);
console.log(1 === '1');
console.log(0 === false);
console.log('juan' === 'juan');
console.log('juan' === juan);

// Relational Operator

//Some comparison operators check whether one value is greater or less than to the other value.

        let a = 50;
        let b = 65;

        //GT or Greater Than operator ( > )
        let isGreaterThan = a > b; // 50 > 65
        //LT or Less Than operator ( < )
        let isLessThan = a < b; // 50 < 65
        //GTE or Greater Than Or Equal operator ( >= ) 
        let isGTorEqual = a >= b; // 50 >= 65
        //LTE or Less Than Or Equal operator ( <= ) 
        let isLTorEqual = a <= b; // 50 <= 65

        //Like our equality comparison operators, relational operators also return boolean which we can save in a variable or use in a conditional statement.
        console.log(isGreaterThan);
        console.log(isLessThan);
        console.log(isGTorEqual);
        console.log(isLTorEqual)

// Logical Operator (&&, ||, !)

let isLegalAge = true;
let isRegistered = false;

// Logical Operator And (&& -Double Ampersand)

let allRequirements = isLegalAge && isRegistered;
console.log("Result of logical AND Operator " + allRequirements);





